const express = require("express");//import thư viện
const logAPIMiddleWare = require("../middleware/middleware");//import middleware
const {
    getAllVouchers,
    getVoucherById,
    createVoucher,
    updateVoucherById,
    deleteVoucherById,
} = require('./../controllers/voucher.controller');//import controller

const voucherRouter = express.Router();//tạo đối tượng router mới
voucherRouter.use(logAPIMiddleWare);//áp dụng function middleware cho toàn bộ router

//Routes CRUD
//get all
voucherRouter.get("/", getAllVouchers);

//get voucher by id
voucherRouter.get("/:voucherId", getVoucherById);

//create voucher 
voucherRouter.post("/", createVoucher);

//update voucher 
voucherRouter.put("/:voucherId", updateVoucherById);

//dalete voucher 
voucherRouter.delete("/:voucherId", deleteVoucherById);

module.exports = voucherRouter;