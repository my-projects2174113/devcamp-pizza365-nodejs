const express = require("express");//import thư viện
const logAPIMiddleWare = require("../middleware/middleware");//import middleware

//import controller
const {
    getAllUsers,
    getAllLimitUsers,
    getAllSkipUsers,
    getAllSortUsers,
    getAllSkipLimitUsers,
    getAllSortSkipLimitUsers,
    getUserById,
    createUser,
    updateUserById,
    deleteUserById
} = require('../controllers/user.controller');

const userRouter = express.Router();//tạo đối tượng router mới
userRouter.use(logAPIMiddleWare);//áp dụng function middleware cho toàn bộ router

//Routes CRUD
//get all
userRouter.get("/", getAllUsers);

//filter user(lưu ý thứ tự đặt url vì express sẽ lấy router từ trên xuống). Ở đây phải đặt trc getById vì có thể gây nhầm lẫn khi thực hiện yêu cầu
//limit
userRouter.get("/limit-users", getAllLimitUsers);
//skip
userRouter.get("/skip-users", getAllSkipUsers);
//sort
userRouter.get("/sort-users", getAllSortUsers);
//skip-limit
userRouter.get("/skip-limit-users", getAllSkipLimitUsers);
//sort-skip-limit
userRouter.get("/sort-skip-limit-users", getAllSortSkipLimitUsers);

//get user by id
userRouter.get("/:userId", getUserById);

//create user 
userRouter.post("/", createUser);

//update user 
userRouter.put("/:userId", updateUserById);

//delete user 
userRouter.delete("/:userId", deleteUserById);

module.exports = userRouter;