//import thư viện
const mongoose = require("mongoose");
//import model
const orderModel = require("../models/order.model");
const userModel = require("../models/user.model");
const voucherModel = require("../models/voucher.model");
const drinkModel = require("../models/drink.model");

//create order
const createOrder = async (req, res) => {
  try {
    //B1: Thu thập dữ liệu
    const { userId, orderCode, pizzaSize, pizzaType, voucher, drink, status } =
      req.body;
    //B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(userId)) {
      //bắt buộc order phải có user
      //kiểm tra có khớp với định dạng objectId ko
      return res.status(400).json({
        message: "UserId is invalid",
      });
    }
    if (!pizzaSize) {
      return res.status(400).json({
        message: "pizzaSize is required",
      });
    }
    if (!pizzaType) {
      return res.status(400).json({
        message: "pizzaType is required",
      });
    }
    if (!mongoose.Types.ObjectId.isValid(voucher) && voucher !== undefined) {
      return res.status(400).json({
        message: "voucher is not valid",
      });
    }
    if (!mongoose.Types.ObjectId.isValid(drink) && drink !== undefined) {
      return res.status(400).json({
        message: "drink is not valid",
      });
    }
    //B3: Xử lý dữ liệu
    //kiểm tra xem drink và voucher có tồn tại ko
    const voucherFound = await voucherModel.findById(voucher);
    if (!voucherFound) {
      return res.status(400).json({
        message: "voucher not found",
      });
    }
    const drinkFound = await drinkModel.findById(drink);
    if (!drinkFound) {
      return res.status(400).json({
        message: "drink not found",
      });
    }
    // Tạo một đơn hàng mới
    let newOrder = { orderCode, pizzaSize, pizzaType, voucher, drink, status }; //tạo đối tượng ko có userId(quản lý đơn hàng theo người dùng)
    let order = await orderModel.create(newOrder); //tạo mới order
    await order.populate('voucher drink');//populate cho 2 thuộc tính voucher và drink

    // Thêm id của đơn hàng mới vào mảng orders của người dùng
    let updateUser = await userModel
      .findByIdAndUpdate(
        userId,
        {
          $push: { orders: order._id }, //$ để phân biệt đây là toán tử
        },
        { new: true }
      )
      .populate("orders");

    // Kiểm tra xem người dùng có tồn tại không
    if (!updateUser) {
      // Nếu không tìm thấy người dùng, trả về lỗi 404
      // 1 đơn hàng bắt buộc phải do ng dùng tạo
      return res.status(404).json({
        message: "User not found",
      });
    }

    //phản hồi
    res.status(201).json({
      message: "Create order successfully",
      data: order,
      user: updateUser,
    });
  } catch (error) {
    console.log(error);
    if (error.code === 11000) {
      // Trường `orderCode` không duy nhất, trả về thông báo lỗi
      return res.status(400).json({ message: "orderCode is unique" });
    }
    // Xử lý các loại lỗi khác
    res.status(500).json({
      message: "Internal Server Error",
    });
  }
};

//get all orders
const getAllOrders = async (req, res) => {
  try {
    //B1: Thu thập dữ liệu
    //B2: Validate dữ liệu
    //B3: Xử lý dữ liệu
    let allOrders = await orderModel.find(); //tìm toàn bộ
    res.status(200).json(allOrders);
  } catch (error) {
    console.log(error);
    res.status(500).json({
      message: "Order not found",
    });
  }
};

//get order by id
const getOrderById = async (req, res) => {
  try {
    //B1: Thu thập dữ liệu
    let orderId = req.params.orderId; //trong params đặt là :id thì .id, đặt là :abc thì .abc
    //B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(orderId)) {
      //kiểm tra có khớp với định dạng objectId ko
      return res.status(400).json({
        message: "Id is invalid",
      });
    }
    //B3: Xử lý dữ liệu
    let order = await orderModel.findById(orderId); //tìm theo id
    if (!order) {
      //nếu ko tìm thấy
      return res.status(404).json({
        message: "Order not found",
      });
    }
    res.status(200).json({
      //phản hồi
      message: "Get order details successfully",
      data: order,
    });
  } catch (error) {
    console.log(error);
    res.status(500).json({
      message: "Internal Server Error",
    });
  }
};

//update order by id
const updateOrderById = async (req, res) => {
  try {
    //B1: Thu thập dữ liệu
    let orderId = req.params.orderId;
    const { orderCode, pizzaSize, pizzaType, voucher, drink, status } =
      req.body;
    //B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(orderId)) {
      //kiểm tra có khớp với định dạng objectId ko
      return res.status(400).json({
        message: "Id is invalid",
      });
    }
    if (!pizzaSize && pizzaSize !== undefined) {
      return res.status(400).json({
        message: "pizzaSize is invalid",
      });
    }
    if (!pizzaType && pizzaType !== undefined) {
      return res.status(400).json({
        message: "pizzaType is invalid",
      });
    }
    if (!mongoose.Types.ObjectId.isValid(voucher) && voucher !== undefined) {
      return res.status(400).json({
        message: "voucher is not valid",
      });
    }
    if (!mongoose.Types.ObjectId.isValid(drink) && drink !== undefined) {
      return res.status(400).json({
        message: "voucher is not valid",
      });
    }
    //B3: Xử lý dữ liệu
    //kiểm tra xem drink và voucher có tồn tại ko
    const voucherFound = await voucherModel.findById(voucher);
    if (!voucherFound) {
      return res.status(400).json({
        message: "voucher not found",
      });
    }
    const drinkFound = await drinkModel.findById(drink);
    if (!drinkFound) {
      return res.status(400).json({
        message: "drink not found",
      });
    }

    let updateOrder = {
      orderCode,
      pizzaSize,
      pizzaType,
      voucher,
      drink,
      status,
    };
    let order = await orderModel.findByIdAndUpdate(orderId, updateOrder, {
      new: true,
    }).populate('voucher', 'drink');
    if (!order) {
      //nếu ko tìm thấy
      return res.status(404).json({
        message: "Order not found",
      });
    }
    res.status(201).json({
      //phản hồi
      message: "Update order successfully",
      data: order,
    });
  } catch (error) {
    console.log(error);
    if (error.code === 11000) {
      // Trường `orderCode` không duy nhất, trả về thông báo lỗi
      return res.status(400).json({ message: "orderCode are unique" });
    }
    // Xử lý các loại lỗi khác
    res.status(500).json({
      message: "Internal Server Error",
    });
  }
};

//delete order by id
const deleteOrderById = async (req, res) => {
  try {
    //B1: Thu thập dữ liệu
    let orderId = req.params.orderId; //trong params đặt là :id thì .id, đặt là :abc thì .abc
    const userId = req.params.userId; //lấy userId trong params
    //B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(orderId)) {
      //kiểm tra có khớp với định dạng objectId ko
      return res.status(400).json({
        message: "Id is invalid",
      });
    }
    if (!mongoose.Types.ObjectId.isValid(userId)) {
      return res.status(400).json({
        message: "userId is invalid",
      });
    }
    //B3: Xử lý dữ liệu
    let order = await orderModel.findByIdAndDelete(orderId); //xóa orders trong collection Orders
    if (!order) {
      //nếu ko tìm thấy
      return res.status(404).json({
        message: "Order not found",
      });
    }

    // Xóa đơn hàng khỏi mảng orders của user
    let updateUser = await userModel
      .findByIdAndUpdate(
        userId,
        {
          $pull: { orders: order._id }, //$ để phân biệt đây là toán tử
        },
        { new: true }
      )
      .populate("orders");

    // Kiểm tra xem người dùng có tồn tại không
    if (!updateUser) {
      // Nếu không tìm thấy người dùng, trả về lỗi 404
      // 1 đơn hàng bắt buộc phải do ng dùng tạo
      return res.status(404).json({
        message: "User not found",
      });
    }

    //phản hồi
    res.status(200).json({
      message: "Delete order successfully",
      data: order,
      user: updateUser,
    });
  } catch (error) {
    console.log(error);
    res.status(500).json({
      message: "Internal Server Error",
    });
  }
};

module.exports = {
  createOrder,
  getAllOrders,
  getOrderById,
  updateOrderById,
  deleteOrderById,
};
