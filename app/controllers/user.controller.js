const mongoose = require("mongoose");
const userModel = require("../models/user.model");

//get all
const getAllUsers = async (req, res) => {
  try {
    //B1: Thu thập dữ liệu
    //B2: Validate dữ liệu
    //B3: Xử lý dữ liệu
    let allUsers = await userModel.find().populate('orders'); //tìm toàn bộ
    res.status(200).json(allUsers);
  } catch (error) {
    console.log(error);
    res.status(500).json({
      message: "Internal Server Error",
    });
  }
};

//get all user limit
const getAllLimitUsers = async (req, res) => {
  try {
    //B1: Thu thập dữ liệu
    const reqLimitUsersString = req.query.limitUsers;
    console.log(reqLimitUsersString)
    const reqLimitUsers = parseInt(reqLimitUsersString);//chuyển query sang number
    //B2: Validate dữ liệu
    if (!Number.isInteger(reqLimitUsers) && reqLimitUsers < 0) {
      return res.status(400).json({
        message: 'limitUsers query is invalid. It must be a positive integer'
      })
    }
    //B3: Xử lý dữ liệu
    let limitedUser = await userModel.find().populate('orders').limit(reqLimitUsers); //tìm "limit" số user đầu tiên
    res.status(200).json({
      //phản hồi
      message: "Get all limited users successfully",
      data: limitedUser,
    });
  } catch (error) {
    console.log(error);
    res.status(500).json({
      message: "Internal Server Error",
    });
  }
};

//get all user skip
const getAllSkipUsers = async (req, res) => {
  try {
    //B1: Thu thập dữ liệu
    const reqSkipUsersString = req.query.skipUsers;
    const reqSkipUsers = parseInt(reqSkipUsersString);//chuyển query sang number
    //B2: Validate dữ liệu
    if (!Number.isInteger(reqSkipUsers) && reqSkipUsers < 0) {
      return res.status(400).json({
        message: 'skipUsers query is invalid. It must be a positive integer'
      })
    }
    //B3: Xử lý dữ liệu
    let skippedUser = await userModel.find().populate('orders').skip(reqSkipUsers); //bỏ qua(skip) reqSkipUsers user đầu tiên
    res.status(200).json({
      //phản hồi
      message: "Get all skipped users successfully",
      data: skippedUser,
    });
  } catch (error) {
    console.log(error);
    res.status(500).json({
      message: "Internal Server Error",
    });
  }
};

//get all user sort
const getAllSortUsers = async (req, res) => {
  try {
    //B1: Thu thập dữ liệu
    //B2: Validate dữ liệu
    //B3: Xử lý dữ liệu
    //tìm toàn bộ và sắp xếp fullName theo alphabet(tăng dần) và phân biệt hoa thường
    let allUsers = await userModel.find().populate('orders').sort({fullName: 1}).collation({locale: 'vi', caseLevel: true}); 
    res.status(200).json({
      //phản hồi
      message: "Get all sorted users successfully",
      data: allUsers,
    });
  } catch (error) {
    console.log(error);
    res.status(500).json({
      message: "Internal Server Error",
    });
  }
};

//get all user limit-skip
const getAllSkipLimitUsers = async (req, res) => {
  try {
    //B1: Thu thập dữ liệu
    const reqLimitUsersString = req.query.limitUsers;
    const reqLimitUsers = parseInt(reqLimitUsersString);//chuyển query sang number
    const reqSkipUsersString = req.query.skipUsers;
    const reqSkipUsers = parseInt(reqSkipUsersString);//chuyển query sang number
    //B2: Validate dữ liệu
    if (!Number.isInteger(reqLimitUsers) && reqLimitUsers < 0) {
      return res.status(400).json({
        message: 'limitUsers query is invalid. It must be a positive integer'
      })
    }
    if (!Number.isInteger(reqSkipUsers) && reqSkipUsers < 0) {
      return res.status(400).json({
        message: 'skipUsers query is invalid. It must be a positive integer'
      })
    }
    //B3: Xử lý dữ liệu
    //bỏ qua reqSkipUsers phần tử đầu và tìm "limit" số user bắt đầu từ reqSkipUsers + 1
    let filteredUser = await userModel.find().populate('orders').skip(reqSkipUsers).limit(reqLimitUsers); 
    res.status(200).json({
      //phản hồi
      message: "Get all filterd users successfully",
      data: filteredUser,
    });
  } catch (error) {
    console.log(error);
    res.status(500).json({
      message: "Internal Server Error", 
    });
  }
};

//get all user limit-skip
const getAllSortSkipLimitUsers = async (req, res) => {
  try {
    //B1: Thu thập dữ liệu
    const reqLimitUsersString = req.query.limitUsers;
    const reqLimitUsers = parseInt(reqLimitUsersString);//chuyển query sang number
    const reqSkipUsersString = req.query.skipUsers;
    const reqSkipUsers = parseInt(reqSkipUsersString);//chuyển query sang number
    //B2: Validate dữ liệu
    if (!Number.isInteger(reqLimitUsers) && reqLimitUsers < 0) {
      return res.status(400).json({
        message: 'limitUsers query is invalid. It must be a positive integer'
      })
    }
    if (!Number.isInteger(reqSkipUsers) && reqSkipUsers < 0) {
      return res.status(400).json({
        message: 'skipUsers query is invalid. It must be a positive integer'
      })
    }
    //B3: Xử lý dữ liệu
    //tìm toàn bộ và sắp xếp fullName theo alphabet(tăng dần) và phân biệt hoa thường
    //bỏ qua reqSkipUsers phần tử đầu và tìm "limit" số user bắt đầu từ reqSkipUsers + 1
    let filteredUser = await userModel.find().populate('orders').sort({fullName: 1}).collation({locale: "en", caseLevel: true}).skip(reqSkipUsers).limit(reqLimitUsers); 
    res.status(200).json({
      //phản hồi
      message: "Get all filtered users successfully",
      data: filteredUser,
    });
  } catch (error) {
    console.log(error);
    res.status(500).json({
      message: "Internal Server Error",
    });
  }
};

//get by id
const getUserById = async (req, res) => {
  try {
    //B1: Thu thập dữ liệu
    let userId = req.params.userId; //trong params đặt là :id thì .id, đặt là :abc thì .abc
    //B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(userId)) {
      //kiểm tra có khớp với định dạng objectId ko
      return res.status(400).json({
        message: "Id is invalid",
      });
    }
    //B3: Xử lý dữ liệu
    let foundUser = await userModel.findById(userId).populate('orders'); //tìm theo id
    if (!foundUser) {
      //nếu ko tìm thấy
      return res.status(404).json({
        message: "User not found",
      });
    }
    res.status(200).json({
      //phản hồi
      message: "Get user details successfully",
      data: foundUser,
    });
  } catch (error) {
    console.log(error);
    res.status(500).json({
      message: "Internal Server Error",
    });
  }
};

//create
const createUser = async (req, res) => {
  try {
    //B1: Thu thập dữ liệu
    const { fullName, email, address, phone, orders } = req.body;
    //B2: Validate dữ liệu
    if (!fullName) {
      return res.status(400).json({
        message: "fullName is required",
      });
    }
    if (!email) {
      return res.status(400).json({
        message: "email is required",
      });
    }
    if (!address) {
      return res.status(400).json({
        message: "address is required",
      });
    }
    if (!phone) {
      return res.status(400).json({
        message: "phone is required",
      });
    }
    //B3: Xử lý dữ liệu
    let newUser = {
      //tạo object(thuộc tính theo định nghĩa model chứ ko phải theo request body, ở đây viết giống cho ngắn gọn thôi)
      fullName,
      email,
      address,
      phone,
      orders,
    };
    let createdUser = await userModel.create(newUser); //thêm mới vào model
    await createdUser.populate('orders');//populate chỉ tương tác đc với dữ liệu đã có sẵn(các hàm find) nên cần chờ tạo xong mới dùng được result.populate()
    res.status(201).json({
      //phản hồi
      message: "Create user successfully",
      data: createdUser,
    });
  } catch (error) {
    console.log(error);
    if (error.code === 11000) {
      // Trường `email & phone` không duy nhất, trả về thông báo lỗi
      return res.status(400).json({ message: "email & phone are unique" });
    }
    // Xử lý các loại lỗi khác
    res.status(500).json({
      message: "Internal Server Error",
    });
  }
};

//update by id
const updateUserById = async (req, res) => {
  try {
    //B1: Thu thập dữ liệu
    let userId = req.params.userId;
    const { fullName, email, address, phone, orders } = req.body;
    //B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(userId)) {
      //kiểm tra có khớp với định dạng objectId ko
      return res.status(400).json({
        message: "Id is invalid",
      });
    }
    if (!fullName && fullName !== undefined) {
      return res.status(400).json({
        message: "fullName is invalid",
      });
    }
    if (!email && email !== undefined) {
      return res.status(400).json({
        message: "email is invalid",
      });
    }
    if (!address && address !== undefined) {
      return res.status(400).json({
        message: "address is invalid",
      });
    }
    if (!phone && phone !== undefined) {
      return res.status(400).json({
        message: "phone is invalid",
      });
    }
    //B3: Xử lý dữ liệu
    let patchUser = {
      //tạo object(thuộc tính theo định nghĩa model chứ ko phải theo request body, ở đây viết giống cho ngắn gọn thôi)
      fullName,
      email,
      address,
      phone,
      orders,
    };
    let updatedUser = await userModel.findByIdAndUpdate(userId, patchUser, {
      new: true,
    }).populate('orders');
    if (!updatedUser) {//nếu ko tìm thấy
      return res.status(404).json({
        message: "User not found",
      });
    }
    res.status(201).json({
      //phản hồi
      message: "Update user successfully",
      data: updatedUser,
    });
  } catch (error) {
    console.log(error);
    if (error.code === 11000) {
      // Trường `email & phone` không duy nhất, trả về thông báo lỗi
      return res.status(400).json({ message: "email & phone are unique" });
    }
    // Xử lý các loại lỗi khác
    res.status(500).json({
      message: "Internal Server Error",
    });
  }
};

//delete by id
const deleteUserById = async (req, res) => {
  try {
    //B1: Thu thập dữ liệu
    let userId = req.params.userId; //trong params đặt là :id thì .id, đặt là :abc thì .abc
    //B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(userId)) {
      //kiểm tra có khớp với định dạng objectId ko
      return res.status(400).json({
        message: "Id is invalid",
      });
    }
    //B3: Xử lý dữ liệu
    let deletedUser = await userModel.findByIdAndDelete(userId).populate('orders'); //tìm theo id
    if (!deletedUser) {
      //nếu ko tìm thấy
      return res.status(404).json({
        message: "User not found",
      });
    }
    res.status(200).json({ //phản hồi
      message: "Delete user successfully",
      data: deletedUser,
    });
  } catch (error) {
    console.log(error);
    res.status(500).json({
      message: "Internal Server Error",
    });
  }
};

module.exports = {
    getAllUsers,
    getAllLimitUsers,
    getAllSkipUsers,
    getAllSortUsers,
    getAllSkipLimitUsers,
    getAllSortSkipLimitUsers,
    getUserById,
    createUser,
    updateUserById,
    deleteUserById
}
