const mongoose = require("mongoose"); //import thư viện
const drinkModel = require("./../models/drink.model"); //import model

//get all
const getAllDrinks = async (req, res) => {
  try {
    //B1: Thu thập dữ liệu
    //B2: Validate dữ liệu
    //B3: Xử lý dữ liệu
    let result = await drinkModel.find(); //tìm toàn bộ
    res.status(200).json({
      //phản hồi
      message: "Lay danh sach nuoc uong thanh cong",
      data: result,
    });
  } catch (error) {
    console.log(error);
    res.status(500).json({
        message: "Khong tim thay nuoc uong",
      });
  }
};

//get by id
const getDrinkById = async (req, res) => {
  try {
    //B1: Thu thập dữ liệu
    let drinkId = req.params.drinkId; //trong params đặt là :id thì .id, đặt là :abc thì .abc
    //B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(drinkId)) {
      //kiểm tra có khớp với định dạng objectId ko
      return res.status(400).json({
        message: "Id khong hop le",
      });
    }
    //B3: Xử lý dữ liệu
    let result = await drinkModel.findById(drinkId); //tìm theo id
    if (!result) {
      //nếu ko tìm thấy
      return res.status(404).json({
        message: "Khong tim thay nuoc uong",
      });
    }
    res.status(200).json({
      //phản hồi
      message: "Lay thong tin nuoc uong thanh cong",
      data: result,
    });
  } catch (error) {
    console.log(error);
    res.status(500).json({
      message: "Internal Server Error",
    });
  }
};

//create
const createDrink = async (req, res) => {
  try {
    //B1: Thu thập dữ liệu
    const { maNuocUong, tenNuocUong, donGia } = req.body;
    //B2: Validate dữ liệu
    if (!maNuocUong) {
      return res.status(400).json({
        message: "Ma nuoc uong khong hop le",
      });
    }
    if (!tenNuocUong) {
      return res.status(400).json({
        message: "Ten nuoc uong khong hop le",
      });
    }
    if (!Number.isInteger(donGia) || donGia < 0) {
      return res.status(400).json({
        message: "Don gia khong hop le",
      });
    }
    //B3: Xử lý dữ liệu
    let newDrink = {
      //tạo object(thuộc tính theo định nghĩa model chứ ko phải theo request body, ở đây viết giống cho ngắn gọn thôi)
      maNuocUong,
      tenNuocUong,
      donGia,
    };
    let result = await drinkModel.create(newDrink); //thêm mới vào model
    res.status(201).json({
      //phản hồi
      message: "Tao nuoc uong thanh cong",
      data: result,
    });
  } catch (error) {
    console.log(error);
    if (error.code === 11000) {
      // Trường `maNuocUong` không duy nhất, trả về thông báo lỗi
      return res
        .status(400)
        .json({ message: "Ma nuoc uong da ton tai trong he thong" });
    }
    // Xử lý các loại lỗi khác
    res.status(500).json({
      message: "Internal Server Error",
    });
  }
};

//update by id
const updateDrinkById = async (req, res) => {
  try {
    //B1: Thu thập dữ liệu
    let drinkId = req.params.drinkId; //trong params đặt là :id thì .id, đặt là :abc thì .abc
    const { maNuocUong, tenNuocUong, donGia } = req.body;
    //B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(drinkId)) {
      //kiểm tra có khớp với định dạng objectId ko
      return res.status(400).json({
        message: "Id khong hop le",
      });
    }
    if (!maNuocUong && maNuocUong !== undefined) {
      //ko update(undefined) thì bỏ qua
      return res.status(400).json({
        message: "Ma nuoc uong khong hop le",
      });
    }
    if (!tenNuocUong && tenNuocUong !== undefined) {
      return res.status(400).json({
        message: "Ten nuoc uong khong hop le",
      });
    }
    if ((!Number.isInteger(donGia) || donGia < 0) && donGia !== undefined) {
      return res.status(400).json({
        message: "Don gia khong hop le",
      });
    }
    //B3: Xử lý dữ liệu
    //hành vi mặc định của Mongoose là loại bỏ các thuộc tính có giá trị là undefined khi thực hiện các hoạt động như tạo mới (create()) hoặc cập nhật (findByIdAndUpdate()).
    let updateDrink = {
      //tạo object(thuộc tính theo định nghĩa model chứ ko phải theo request body, ở đây viết giống cho ngắn gọn thôi)
      maNuocUong,
      tenNuocUong,
      donGia,
    };
    let result = await drinkModel.findByIdAndUpdate(drinkId, updateDrink); //update by id
    if (!result) {
      //nếu ko tìm thấy
      return res.status(404).json({
        message: "Khong tim thay nuoc uong",
      });
    }
    res.status(200).json({
      //phản hồi
      message: "Cap nhat nuoc uong thanh cong",
      data: result,
    });
  } catch (error) {
    console.log(error);
    if (error.code === 11000) {
      // Trường `maNuocUong` không duy nhất, trả về thông báo lỗi
      return res
        .status(400)
        .json({ message: "Ma nuoc uong da ton tai trong he thong" });
    }
    // Xử lý các loại lỗi khác
    res.status(500).json({
      message: "Internal Server Error",
    });
  }
};

//delete by id
const deleteDrinkById = async (req, res) => {
  try {
    //B1: Thu thập dữ liệu
    let drinkId = req.params.drinkId; //trong params đặt là :id thì .id, đặt là :abc thì .abc
    //B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(drinkId)) {
      //kiểm tra có khớp với định dạng objectId ko
      return res.status(400).json({
        message: "Id khong hop le",
      });
    }
    //B3: Xử lý dữ liệu
    let result = await drinkModel.findByIdAndDelete(drinkId); //tìm theo id
    if (!result) {
      //nếu ko tìm thấy
      return res.status(404).json({
        message: "Khong tim thay nuoc uong",
      });
    }
    res.status(200).json({
      //phản hồi 200. Delete chuyên dùng 204 No Content res.status(200).json(); Nhưng thiếu tính trực quan cho ng dùng nên đổi sang 200
      message: "Xoa nuoc uong thanh cong",
      data: result,
    });
  } catch (error) {
    console.log(error);
    res.status(500).json({
      message: "Internal Server Error",
    });
  }
};

module.exports = {
  getAllDrinks,
  getDrinkById,
  createDrink,
  updateDrinkById,
  deleteDrinkById,
};
